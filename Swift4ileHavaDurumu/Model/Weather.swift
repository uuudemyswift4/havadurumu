//
//  Weather.swift
//  Swift4ileHavaDurumu
//
//  Created by Kerim Çağlar on 13/07/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import Foundation

struct WeathearData:Codable{
    
    var currently : Currently
    
    struct Currently:Codable {
        
        var summary:String
        var apparentTemperature:Double //Fahrenayt -> Celcius
        
        var tempCelcius:String{
            let celcius = String(((apparentTemperature-32)*5/9).sayiyiYuvarla(basamak: 2))
            return celcius
        }
    }
}

extension Double{
    
    func sayiyiYuvarla(basamak:Int)->Double{
        
        let carpan = pow(10.0, Double(basamak))
        
        return (self*carpan).rounded()/carpan
        
    }
    
}
