//
//  ViewController.swift
//  Swift4ileHavaDurumu
//
//  Created by Kerim Çağlar on 13/07/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import UIKit
import MapKit


class ViewController: UIViewController, CLLocationManagerDelegate {
    
    var manager = CLLocationManager()
    
    @IBOutlet weak var summary: UILabel!
    
    @IBOutlet weak var temp: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        summary.text = ".."
        temp.text = "..."
        
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization() //info.plist izni gerekir
        manager.startUpdatingLocation()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = CLLocationCoordinate2D(latitude: locations[0].coordinate.latitude, longitude: locations[0].coordinate.longitude)
        manager.stopUpdatingLocation()
        let currentLocation = location.latitude.description + "," + location.longitude.description
        
        let myStringUrl = "https://api.darksky.net/forecast/9ed6b427c70f6f622b5df35df73b6117/" + currentLocation + "?lang=tr"
        let url = URL(string:myStringUrl)!
        let myData = try! Data(contentsOf:url)
        let jsonDecoder = JSONDecoder()
        
        let result = try? jsonDecoder.decode(WeathearData.self, from: myData)
        
        if let havaDurumu = result?.currently{
            print("Ortalama Sıcaklık:\(havaDurumu.apparentTemperature)")
            summary.text = havaDurumu.summary
            temp.text = havaDurumu.tempCelcius
        }
        
    }

}

